import { isLeapYear } from '@/utils/isLeapYear';
import { startDay } from '@/utils/startDay';

export const weekArray = year => {
	let array = []

	for (let i = 0; i <= 6; i++) {
		array.push(52)
	}

	let day = startDay(year)

	array[day] += 1

	const isLeap = isLeapYear(year)

	if (isLeap === true) {
		array[(day+1)%7] += 1
	}

	return array
}