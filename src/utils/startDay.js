// source
const mod = (num) => {
	// define start day of cycle (saturday)
	const startCompensation = 6

	// limit number to 400 range
	num%=400

	// calculate passed leap day amount
	let amount1 = Math.floor((num+3)/4)

	// calculate century leap day compensation
	let amount2 = Math.floor((num-1)/100)

	// prevent amount2 from going negative
	// this can also be done by wrapping the amount2 definition in Math.max(0,n)
	// but i'll keep it like this to maintain readability
	amount2 = amount2 < 0 ? 0 : amount2

	// put everything together and limit the range to 7 days
	return (num + amount1 - amount2 + startCompensation) % 7
}

// concatenated version, does the exact same thing but causes incurable headaches when trying to understand...
export const startDay = x => ((x%=400)+(Math.floor((x+3)/4))-(x>300?3:x>200?2:x>100?1:0)+6)%7