// this actually does not correctly return the right value
// it is supposed to return the passed leap day on the next common year after the leap year
// but it returns the passed leap day only on the next leap year
// which is incorrect and therefore it cannot be used for accurate calculations
export const leapDaysPassed = year => {
	const base4 = Math.floor(year/4);
	const base100 = Math.floor(year/100);
	const base400 = Math.floor(year/400);

	return base4 - base100 + base400
}