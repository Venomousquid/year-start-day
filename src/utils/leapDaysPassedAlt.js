// this actually does not correctly return the right value
// it is supposed to return the passed leap day on the next common year after the leap year
// but it returns the passed leap day only on the next leap year
// which is incorrect and therefore it cannot be used for accurate calculations
export const leapDaysPassedAlt = year => {
	const magicNumber = 10000/2425;
	return Math.round(--year/magicNumber)
}