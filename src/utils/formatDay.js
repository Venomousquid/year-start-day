const days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
export const formatDay = (day, plural = false) => {
	return `${days[day]}${plural ? 's' : ''}`
}